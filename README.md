# Design challenge - Nest Collective Summer Internship 2020

Design and build a responsive web page for Nest Collective.

At Nest Collective, we are a collective of product studios who have created a new model for company collaboration.

The main goal for this challenge is to build a layout for a homepage that let the users know more about us. Think about what layout, animations, and functionalities would be more effective to impress and appeal the users!


### Important information
Feel free to add *"Lorem ipsum"* text in any section, to choose fonts and colors, to add other images, other sections to the homepage, if you think that can add value.

#### Navigation

Mandatory links

* About
* Companies
* Events


#### Companies section

Use this [list of logos](https://gitlab.com/nest-collective/nest-si-2020-design-challenge/-/tree/master/Nest%20Collective%20-%20Companies%20logos).


#### Events section

Use this [list of sample events](https://docs.google.com/spreadsheets/d/1eshQxUyGckqA7-g4ovtPQ-AaZ4JuEAX5Go9VDF34g5E/). Note: you don’t have to display all the events above, nor all its info.


#### Footer section

Mandatory Content

* Social (Medium, Twitter, Facebook, Instagram, Linkedin)
* Addresses


### Important information

#### 1. Research
Research similar solutions

#### 2. Wireframe + Mockup
Design a wireframe + high-fidelity mockup

#### 3. Build
Build a prototype using HTML and CSS *(Can also use SCSS, SASS, LESS or other type of styling)*

*We suggest you to use half of your time to tackle the first 2 points (Research and Wireframes) and the other half to the last one (Build). But feel free to manage your time as you want.*

---

> Be bold, innovative, and think outside of the box! Don't play safe,  mindblow us!

---


Use whatever tools you're more comfortable with. Afterwards, you should fork this repository and upload your work there to share it with us.

Feel free to ask us any questions you have during the challenge. At the end, we’ll meet to discuss your process, results and decisions.

Good luck and have fun! 😉
